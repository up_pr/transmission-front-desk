import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)
const originalPush = Router.prototype.push

Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}

//静态路由
let staticRoute = [{
  path: "/login",
  name: "login",
  component: () => import("@/page/login.vue")
},{
  path: "/total",
  name: "total",
  component: () => import("@/page/total.vue")
},{
  path: "/noAuth",
  name: "noAuth",
  component: () => import("@/page/404.vue")
}]
//动态路由
let dynamicRoute = [
  {
  path: '/',
  name: 'home',
  redirect: "/home",
  component: () => import("@/layout/index.vue"),
  meta:{
    title:"首页",
    icon:"el-icon-s-home",
  },
  children: [
      {
        path: "/home",
        name: "home",
        meta:{
          title:"首页",
          icon:"el-icon-s-data",
          isAffix:true,
          keepAlive:true,
        },
        component: () => import("@/page/home/home.vue")
      },
    {
      path: "/account_group",
      name: "account_group",
      component: () => import("@/page/account_group/index.vue"),
      meta:{
        title:"账号分组",
        keepAlive:true,
        icon:"el-icon-folder-opened",
      }
    },{
      path: "/account",
      name: "account",
      component: () => import("@/page/account/index.vue"),
      meta:{
        title:"账号列表",
        keepAlive:true,
        icon:"el-icon-user",
      }
    },
    {
      path: "/product_goryp",
      name: "product_goryp",
      component: () => import("@/page/product_goryp/index.vue"),
      meta:{
        title:"产品分组",
        keepAlive:true,
        icon:"el-icon-folder-opened",
      }
    },{
      path: "/product",
      name: "product",
      component: () => import("@/page/product/index.vue"),
      meta:{
        title:"产品列表",
        keepAlive:true,
        icon:"el-icon-goods",
      }
    },
    {
        path: "/city_goryp",
        name: "city_goryp",
        component: () => import("@/page/city_goryp/index.vue"),
        meta:{
          title:"城市分组",
          keepAlive:true,
          icon:"el-icon-folder-opened",
        }
    },{
          path: "/city",
          name: "city",
          component: () => import("@/page/city/index.vue"),
          meta:{
            title:"城市列表",
            keepAlive:true,
            icon:"el-icon-office-building",
          }
    },
    {
      path: '/msg_base',
      name: 'msg_base',
      redirect: "/msg_base/msg_base",
      component: () => import("@/layout/routerView/parent"),
      meta:{
        title:"话术",
        icon:"el-icon-folder",
      },
      children: [
        {
          path: "/msg_base",
          name: "msg_base",
          component: () => import("@/page/msg_base/index.vue"),
          meta:{
            title:"话术列表",
            keepAlive:true,
            icon:"el-icon-s-order",
          }
        },
        {
          path: "/msg_config",
          name: "msg_config",
          component: () => import("@/page/msg_base/config.vue"),
          meta:{
            title:"话术配置",
            keepAlive:true,
            icon:"el-icon-s-tools",
          }
        }
      ]
    },
    {
      path: "/domain",
      name: "dmain",
      component: () => import("@/page/dmain/index.vue"),
      meta:{
        title:"域名列表",
        keepAlive:true,
        icon:"el-icon-user",
      }
    },{
      path: "/err_check",
      name: "err_check",
      component: () => import("@/page/err_check/index.vue"),
      meta:{
        title:"异常账号",
        keepAlive:true,
        icon:"el-icon-folder-opened",
      }
    }
        // {
        //   path: "/city",
        //   name: "city",
        //   component: () => import("@/page/city/index.vue"),
        //   meta:{
        //     title:"城市列表",
        //     keepAlive:true,
        //     icon:"el-icon-s-tools",
        //   }
        // }
    // {
    //   path: "/ip_var",
    //   name: "ip_var",
    //   component: () => import("@/page/ip_Var/index.vue"),
    //   meta:{
    //     title:"IP管理",
    //     keepAlive:true,
    //     icon:"el-icon-user",
    //   }
    // },
    // {
    //   path: "/short",
    //   name: "shorturl",
    //   component: () => import("@/page/short/index.vue"),
    //   meta:{
    //     title:"生成短链接",
    //     keepAlive:true,
    //     icon:"el-icon-office-building",
    //   }
    // },
  ]
}]
export {dynamicRoute}

let routes = dynamicRoute.concat(staticRoute)
export default new Router({
  routes,
})
