import * as apibase from "./apiRoot"
import qs from 'qs'
import request from "../utils/request"
import {img_base} from "./apiRoot";
function Post(router,data){
    return request({
        method:"post",
        data,
        url:apibase.baseUrl+router,
    })
}
function Post2(router,data){
    return request({
        method:"post",
        data,
        url:apibase.DocsBase+router,
    })
}
function Check(router,query,header = {}){
    return request({
        url: apibase.baseUrl+router,
        method: 'get',
        params:query,
        headers:header,
    })
}
function Check2(router,query,header = {}){
    return request({
        url: apibase.DocsBase+router,
        method: 'get',
        params:query,
        headers:header,
    })
}
function OpenBiter(id){
    return request({
        method:"post",
        data:{id:id},
        url:"http://127.0.0.1:54345/browser/open",
    })
}

function upfile(data){
    const formData = new FormData()
    for(let k in data){
        formData.append(k, data[k]);
    }
    return request({
        method:"post",
        data:formData,
        headers: {
            'Content-Type': 'multipart/form-data'
        },
        url:apibase.img_base,
    })
}
function upfileTarget(){
    return apibase.img_base
}

//====================


function get_city(data){
    return Check("/city/",data)
}
function create_city(data){
    return Post("/city/",data)
}
function delte_city(data){
    return Post("/city/del",{ids:data})
}
function update_city(data){
    return Post("/city/update",data)
}
function downCityTempate(){
    return apibase.baseUrl+"/city/down"
}
function upCityTempate(){
    return apibase.baseUrl+"/city/up_tempate"
}




function get_cg(data){
    return Check("/cg/",data)
}
function get_cg_list(data){
    return Check("/cg/list",data)
}
function create_cg(data){
    return Post("/cg/",data)
}
function delte_cg(data){
    return Post("/cg/del",{ids:data})
}
function update_cg(data){
    return Post("/cg/update",data)
}


function get_ag(data){
    return Check("/account_group/",data)
}
function create_ag(data){
    return Post("/account_group/",data)
}
function delte_ag(data){
    return Post("/account_group/del",{ids:data})
}
function update_ag(data){
    return Post("/account_group/update",data)
}


function get_ip_var(data){
    return Check("/ip_var/",data)
}
function create_ip_var(data){
    return Post("/ip_var/",data)
}
function delte_ip_var(data){
    return Post("/ip_var/del",{ids:data})
}
function update_ip_var(data){
    return Post("/ip_var/update",data)
}
function updates_ip_var(data){
    return Post("/ip_var/upadtes",data)
}

function get_account(data){
    return Check("/account/",data)
}
function get_account_task(data){
    return Check("/account/task",data)
}
function create_account(data){
    return Post("/account/",data)
}
function delte_account(data){
    return Post("/account/del",{ids:data})
}
function update_account(data){
    return Post("/account/update",data)
}

function update_account_template(){
    return apibase.baseUrl+"/account/update_account"
}


function reset_account(data){
    return Check("/account/reset",data)
}
function downAccountTempate(){
    return apibase.baseUrl+"/account/down"
}
function lowFunc(data){
    return Post("/account/low",data)
}
function home_total(data){
    return Check("/account/home_total",data)
}
function update_group(data){
    return Post("/account/update_group",data)
}

function update_state(data){
    return Post("/position_fun/update_state",data)
}


function flow_total(data){
    return Check("/account/flow_total",data)
}
function flow_account_state(data){
    return Check("/account/flow_account_state",data)
}


function outTask(data){
    return Post("/account/out_task",data)
}
function runTask(data){
    return Post("/account/run_task",data)
}


function verify_account(data){
    return Post("/account/verify_account",{ids:data})
}
function update_methods(data){
    return Post("/account/update_methods",data)
}
function msg_total(data){
    return Check("/account/msg_total",data)
}
function flow_account_reply(data){
    return Check("/account/flow_account_reply",data)
}
function flow_product(data){
    return Check("/account/flow_product",data)
}
function position_total(data){
    return Check("/account/position_total",data)
}

function upAccountTempate(){
    // const formData = new FormData()
    // for(let k in data){
    //     formData.append(k, data[k]);
    // }
    // return request({
    //     method:"post",
    //     data:formData,
    //     headers: {
    //         'Content-Type': 'multipart/form-data'
    //     },
    //     url:apibase.baseUrl+"/account/up_tempate",
    // })
    return apibase.baseUrl+"/account/up_tempate"
}

function upUpdateProxyTemplate(){
    return apibase.baseUrl+"/account/ExportUpdateProxyTemplate"
}
function UpdateProxy(){
    return apibase.baseUrl+"/account/UpdateProxy"
}
function CloerProxy(){
    return Post("/account/CloerProxy")
}


function get_product(data){
    return Check("/product/",data)
}
function create_product(data){
    return Post("/product/",data)
}
function delte_product(data){
    return Post("/product/del",{ids:data})
}
function update_product(data){
    return Post("/product/update",data)
}
function downProductTempate(){
    return apibase.baseUrl+"/product/down"
}
function upProductTempate(){
    return apibase.baseUrl+"/product/up_tempate"
}

function login_func(data){
    return Post("/login_fun/",data)
}
function position_fun(data){
    return Post("/position_fun/",data)
}
function pass_preview(data){
	 return Post("/pass/preview",data)
}
function del_pass_product(data){
    return Post("/pass/del_pass_product",data)
}

function pass_func(data,agid){
	 return Post("/pass/",data)
}
function pass_del_product(data){
    return Post("/pass/del",data)
}

function get_product_group(data){
    return Check("/product_group/",data)
}
function create_product_group(data){
    return Post("/product_group/",data)
}
function delte_product_group(data){
    return Post("/product_group/del",{ids:data})
}
function update_product_group(data){
    return Post("/product_group/update",data)
}
function short_post(data){
    return Post("/short/",data)
}

function reply_post(data){
    return Post("/reply/",data)
}
function reply_stop(data){
    return Post("/reply/stop",data)
}
function reply_check(data){
    return Post("/reply/check",data)
}

function get_msg_base(data){
    return Check("/msg_base/",data)
}
function create_msg_base(data){
    return Post("/msg_base/",data)
}
function delte_msg_base(data){
    return Post("/msg_base/del",{ids:data})
}
function update_msg_base(data){
    return Post("/msg_base/update",data)
}

function get_Msg_select(data){
    return Check("/account/msg_select",data)
}

function get_dmain(data){
    return Check("/dmain/",data)
}
function create_dmain(data){
    return Post("/dmain/",data)
}
function delte_dmain(data){
    return Post("/dmain/del",{ids:data})
}
function update_dmain(data){
    return Post("/dmain/update",data)
}
function short_request_date(){
    return Check("/short/total",null)
}

function down_export_account(){
    return apibase.baseUrl+"/account/dl"
}
function downAccountPassTempate(){
    return apibase.baseUrl+"/account/up_pass_template"
}

function upAccountPassTempate(){
    return apibase.baseUrl+"/account/temp_pass"
}

function OpenSendMssage(){
    return Check("/reply/open_send_msg",null)
}
function OpenMssageList(){
    return Check("/reply/open_msg_list",null)
}
function OpenSytProxy(){
    return Check("/reply/open_syt_proxy",null)
}
function CheckMsgConfig(){
    return Check("/reply/msg_config",null)
}

function AccountErrCheck(data){
    return Check("/account/err_check",data)
}

function CheckUpdateProduct(data){
    return Post("/product/all_update_cartegory",data)
}

function GetDocsUserList(data){
    return Check2("/api/user/",data)
}

function GetMsgBaseConfig(){
    return Check("/msg_base_config/read")
}

function SaveMsgBaseConfig(data){
    return Post("/msg_base_config/save",data)
}

function SaveMsgBase(data){
    return Post("/msg_base/save",data)
}

function Wheel_statistics(data){
    return Check("/msg_base/wheel_statistics",data)
}


//启动改主页任务
function RunUpdateHomePage(data){
    return Post("/update_home_page/run",data)
}
//停止改主页任务
function StopUpdateHomePage(data){
    return Post("/update_home_page/edite",{
        Ids:data.Ids,
        State:1,
    })
}
//重试该主页任务
function ResetUpdateHomePage(data){
    return Post("/update_home_page/edite",{
        Ids:data.Ids,
        State:5,
    })
}


function OneRunTask(data){
    return Post("/account/one_run_task",data)
}

export default {
    GetDocsUserList,
    OneRunTask,
    OpenSytProxy,
    UpdateProxy,
    CloerProxy,
    upUpdateProxyTemplate,
    RunUpdateHomePage,
    StopUpdateHomePage,
    ResetUpdateHomePage,
    GetMsgBaseConfig,
    SaveMsgBaseConfig,
    Wheel_statistics,
    SaveMsgBase,
    CheckUpdateProduct,
    AccountErrCheck,
    OpenSendMssage,
    CheckMsgConfig,
    OpenMssageList,
    get_ip_var,
    create_ip_var,
    delte_ip_var,
    update_ip_var,
    "base":apibase.baseTarget,
    get_dmain,
    create_dmain,
    down_export_account,downAccountPassTempate,upAccountPassTempate,
    downCityTempate,
    update_state,
    upCityTempate,
    delte_dmain,
    flow_total,
    upProductTempate,
    downProductTempate,
    updates_ip_var,
    short_request_date,
    flow_account_reply,
    update_dmain,
    get_msg_base,
    update_account_template,
    create_msg_base,
    flow_product,
    flow_account_state,
    position_total,
    delte_msg_base,
    get_Msg_select,
    update_msg_base,
    reply_stop,
    reply_check,
    verify_account,
    home_total,
    login_func,
    update_methods,
    msg_total,
	position_fun,
    get_product_group,
    create_product_group,
    delte_product_group,
    update_product_group,
    del_pass_product,
	pass_func,
    get_product,
    reset_account,
	pass_preview,
    create_product,
    pass_del_product,
    delte_product,
    update_product,
    lowFunc,
    upfile,
    upfileTarget,
    get_cg,
	get_account_task,
    update_group,
    create_cg,
    outTask,
    delte_cg,
    runTask,
    update_cg,
    get_account,
    create_account,
    delte_account,
    update_account,
    get_city,
    create_city,
    delte_city,
    update_city,
    get_ag,
    create_ag,
    delte_ag,
    update_ag,
    downAccountTempate,
    upAccountTempate,
	get_cg_list,
    OpenBiter,
    short_post,
    reply_post,
}
