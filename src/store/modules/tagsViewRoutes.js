const tagsViewRoutesModule = {
    namespaced: true,
    state: {
        tagsViewRoutes: [],
    },
    mutations: {
        // 设置 TagsView 路由
        getTagsViewRoutes(state, data) {
            state.tagsViewRoutes = data;
        },
    },
    actions: {
        // 设置 TagsView 路由
        async setTagsViewRoutes({ commit }, data) {
            commit('getTagsViewRoutes', data);
        },
        async setTagsViewRoutesByRoutes({ commit }, arr){
            if (arr.length <= 0) commit('getTagsViewRoutes', []);
            for (let i = 0; i < arr.length; i++) {
                if (arr[i].children) {
                    arr = arr.slice(0, i + 1).concat(arr[i].children, arr.slice(i + 1));
                }
            }
            commit('getTagsViewRoutes', arr);
        }
    },
};

export default tagsViewRoutesModule;
