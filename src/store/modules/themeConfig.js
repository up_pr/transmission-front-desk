//collapse
const themeConfigModule = {
  namespaced: true,
  state: {
    themeConfig: {
      // 是否开启菜单水平折叠效果
      isCollapse: false,
      // 是否开启tageNav缓存
      isCacheTagsView: true,
	  //项目标题
	  globalTitle:"上品我来帮忙",

    },
  },
  mutations: {
    // 设置布局配置
    getThemeConfig(state, data) {
      state.themeConfig = data;
    },
  },
  actions: {
    // 设置布局配置`
    setThemeConfig({ commit }, data) {
      commit('getThemeConfig', data);
    },
  },
};

export default themeConfigModule;
