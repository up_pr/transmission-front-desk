const userInfo = {
  namespaced: true,
  state: {
    userInfo:{
		name:"admin",
		right:[],
		token:"",
	}
  },
  mutations: {
   submit_user_info(state,data={}){
	   state.userInfo = data
   }
  },
  actions: {
   setUserInfo({commit},data){
	commit("submit_user_info",data)   
   }
  },
};

export default userInfo;
