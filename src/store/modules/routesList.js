const routesList = {
  namespaced: true,
  state: {
    routesList:[],
  },
  mutations: {
    subRoutesList(state,data){
      state.routesList = data
    }
  },
  actions: {
    setRoutesList({commit},data){
      commit("subRoutesList",data)
    }
  },
}
export default routesList
