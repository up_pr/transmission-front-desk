import Vue from 'vue'
import App from './App'
import router from './router'
import Element from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import store from "./store"
import '@/theme/index.scss';
//登录界面粒子background
import Particles from 'vue-particles';
import {dynamicRoute} from "./router/index"
import VueQuillEditor from 'vue-quill-editor'
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'
Vue.use(VueQuillEditor)
import Contextmenu from "vue-contextmenujs"
Vue.use(Contextmenu);
//实现quill-editor编辑器拖拽上传图片
import * as Quill from 'quill'
import { ImageDrop } from 'quill-image-drop-module'
Quill.register('modules/imageDrop', ImageDrop)
console.log("Hello DJ")
//实现quill-editor编辑器调整图片尺寸
import ImageResize from 'quill-image-resize-module'
Quill.register('modules/imageResize', ImageResize)

Vue.config.productionTip = false
Vue.use(Element)
Vue.use(Particles)
store.dispatch("routesList/setRoutesList",dynamicRoute[0].children)
//转换路由转换成一维数组方便tagsRoute
store.dispatch("tagsViewRoutes/setTagsViewRoutesByRoutes",dynamicRoute[0].children)
import {Local, Session} from '@/utils/storage';
Vue.prototype.bus = new Vue();
//路由全局守卫更新Title
router.beforeEach((to, from, next) => {
  document.title = to.meta.title || store.state.themeConfig.themeConfig.globalTitle
  //登录验证
    let isAuthenticated = Local.get("token")
    if (to.name !== 'login' && !isAuthenticated) next({ name: 'login' })
    else next()
})
//读取登录缓存

store.commit('userInfo/submit_user_info', Local.get("userInfo"));
//全局组件
import topoption from '@/components/page/select_top_bar'
Vue.component('top-option',topoption)
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})


