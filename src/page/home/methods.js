const exportFile = function(list,filename){
    let excelList = [];
    let chuTHisTh = ["品牌", "ASIN", "TOP"];
    for (var i = 0; i < chuTHisTh.length; i++) {
        excelList.push(chuTHisTh[i] + ",");
    }
    excelList.push('\n')
    for (var i = 0; i < list.length; i++) {
        let item = list[i]
        excelList.push(item.brand + ",")
        excelList.push(item.Asin + ",")
        excelList.push(item.Top + ",")
        excelList.push("\n");
    }
    var merged = excelList.join("");
    let uri = 'data:text/csv;charset=utf-8,\ufeff' + encodeURIComponent(merged);
    let link = document.createElement("a");
    link.href = uri;
    link.download = `${filename}.csv`;
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
}
export default {
    exportFile
}