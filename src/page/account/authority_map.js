var authority_list = {
    "login":["Account_name","Ag_name","State","Run_at","Msg","CreatedAt","Login_func","OutTask","ResetTask","State_select"],
    "position":["position_run_ip_new","position_run_at","Run_Position","Account_name","Ag_name","State","Run_at","Msg","City_name","CreatedAt","Position_func","OutTask","ResetTask","Verify_func","State_select"],
    "delete_product":["delete_product_run_at","delete_product","Account_name","Ag_name","State","Run_at","Msg","City_name","CreatedAt","Position_func","OutTask","ResetTask","Verify_func","State_select"],
    "update_cookie":["update_cookie_run_ip_new","Update_cookie_run_at","update_cookie","Account_name","Ag_name","State","Run_at","Msg","City_name","CreatedAt","Position_func","OutTask","ResetTask","Verify_func","State_select"],
    "pass":["pass_run_ip_new","pass_run_at","Account_name","Ag_name","State","Run_at","Msg","City_name","Task_total","CreatedAt","Pass_func","OutTask","ResetTask","Verify_func","State_select"],
    "ReplyMesssage":["list_run_ip_new","Reply_tongji","Send_msg_run_at","Msg_list_run_at","Account_name","Reply_total","Reply_now_total","Task_total","Ag_name","Reply_state","Msg","City_name","Reply_number","CreatedAt","Msg_run_func","Msg_Stop_func","State_select"],
    "verify":["Account_name","Ag_name","verify_total","State","Msg","City_name","CreatedAt","Verify_func","State_select","OutTask","Verify_at","Task_total"],
    "query":["Account_name","delete_product_run_at","Update_home_page_run_at","errmsg","pass_run_at","position_run_at","Send_msg_run_at","Msg_list_run_at","Ag_name","Msg","City_name","CreatedAt","Methods_select","Msg_details"],
    "update_home_page":["Update_home_page_city","Update_home_page_run_at","State","Update_home_page_submit_at","Account_name","errmsg","Ag_name","Msg","City_name","CreatedAt","Methods_select","Msg_details","State_select"],
    "alive":["NewReply","Reply_tongji","Send_msg_run_at","Msg_list_run_at","Account_name","Reply_total","Reply_now_total","Task_total","Ag_name","Reply_state","Msg","City_name","Reply_number","CreatedAt","Msg_run_func","Msg_Stop_func","State_select"],
    "ReplyNow":["send_run_ip_new","NewReply","Reply_tongji","Send_msg_run_at","Msg_list_run_at","Account_name","Reply_total","Reply_now_total","Task_total","Ag_name","Reply_state","Msg","City_name","Reply_number","CreatedAt","Msg_run_func","Msg_Stop_func","State_select"],
}
var authority_state = {
    "login1":["open_biter","run_login","remover_login","remover_position","remover_pass","remover_verify","remover_reply"],
    "login2":["open_biter","out_task"],
    "login3":["open_biter","remover_login","remover_position","remover_pass","remover_verify","remover_reply"],
    "login4":["open_biter","remover_query","Reset_task"],
    "login5":["open_biter"],
    "login7":["open_biter"],

    "position1":["Position_run","run_login","remover_login","remover_position","remover_pass","remover_verify","remover_reply"],
    "position2":["Out_task","out_task"],
    "position3":["Position_run","remover_login","remover_position","remover_pass","remover_verify","remover_reply"],
    "position4":["Reset_task","Position_run","Reset_task"],
    "position5":["Out_task"],
    "position7":["open_biter"],

    "delete_product1":["delete_product","run_login","remover_login","remover_position","remover_pass","remover_verify","remover_reply"],
    "delete_product2":["Out_task","out_task"],
    "delete_product3":["delete_product","remover_login","remover_position","remover_pass","remover_verify","remover_reply"],
    "delete_product4":["Reset_task","delete_product","Reset_task"],
    "delete_product5":["Out_task"],
    "delete_product7":["open_biter"],

    "update_cookie1":["update_cookie","run_login","remover_login","remover_position","remover_pass","remover_verify","remover_reply"],
    "update_cookie2":["Out_task","out_task"],
    "update_cookie3":["update_cookie","remover_login","remover_position","remover_pass","remover_verify","remover_reply"],
    "update_cookie4":["Reset_task","delete_product","Reset_task"],
    "update_cookie5":["Out_task"],
    "update_cookie8":["open_biter"],
    "update_cookie7":["open_biter"],


    "pass1":["Pass_run","run_login","remover_login","remover_position","remover_pass","remover_verify","remover_reply"],
    "pass2":["open_biter","Out_task"],
    "pass3":["Pass_run","remover_login","remover_position","remover_pass","remover_verify","remover_reply"],
    "pass4":["Pass_run","Reset_task","Reset_task"],
    "pass5":["Out_task"],
    "pass7":["open_biter"],

    "verify1":["open_biter","run_login","remover_login","remover_position","remover_pass","remover_verify","remover_reply"],
    "verify2":["open_biter","out_task"],
    "verify3":["open_biter","remover_login","remover_position","remover_pass","remover_verify","remover_reply"],
    "verify4":["open_biter","remover_query","remover_verify"],
    "verify5":["open_biter"],
    "verify7":["open_biter"],

    "ReplyMesssage1":[],
    "ReplyMesssage2":["Msg_Stop_func"],
    "ReplyMesssage3":[],
    "ReplyMesssage4":[],
    "ReplyMesssage5":[],
    "ReplyMesssage7":[],

    "query1":["remover_login","remover_position","remover_pass","remover_verify"],
    "query2":["remover_login","remover_position","remover_pass","remover_verify"],
    "query3":["remover_login","remover_position","remover_pass","remover_verify"],
    "query4":["remover_login","remover_position","remover_pass","remover_verify"],
    "query5":["remover_login","remover_position","remover_pass","remover_verify"],
    "query6":["remover_login","remover_position","remover_pass","remover_verify"],
    "query7":["remover_login","remover_position","remover_pass","remover_verify"],
    "query8":["remover_login","remover_position","remover_pass","remover_verify"],
    "query9":["remover_login","remover_position","remover_pass","remover_verify"],

    "ReplyNow1":["open_biter","Msg_run"],
    "ReplyNow2":["open_biter","Msg_Stop_func"],
    "ReplyNow3":["open_biter","remover_login","remover_position","remover_pass","remover_verify","remover_reply"],
    "ReplyNow4":["open_biter","Msg_run"],
    "ReplyNow5":["open_biter","Msg_Stop_func"],
    "ReplyNow7":["open_biter"],


    "update_home_page1":["RunUpdateHomePage"],
    "update_home_page2":["open_biter","CloseUpdateHomePage"],
    "update_home_page3":["open_biter","RunUpdateHomePage","remover_position","remover_pass","remover_verify","remover_reply"],
    "update_home_page4":["open_biter","ResetUpdateHomePage","CloseUpdateHomePage","RunUpdateHomePage"],
    "update_home_page5":["open_biter","CloseUpdateHomePage"],
    "update_home_page7":["open_biter"],
}


export default {
    authority_list,
    authority_state
}




